const Factory = require('./factory');
const Garage = require('./garage');

class GarageFactory extends Factory {
  constructor(carFactory) {
    super();

    this.supportedTypes = {
      random: true
    };
  }

  generate(type) {
    super.generate(type);

    if(type === 'random') {
      return this.createRandomGarage();
    }
  }

  createRandomGarage() {
    return new Garage();
  }
}

module.exports = GarageFactory;