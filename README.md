#Car factory game

##Description
The app creates a bunch of 'cars' and 'garages', pushes those cars to a random garage.
On each step cars are moving to the target parking slot within the garage.
The process ends when all the cars take their places.

##Prerequisites
* node.js v. >=8
* npm v. >=6

##Setup
`npm i`

##Run
`npm start`