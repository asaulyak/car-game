module.exports = Object.freeze({
  garage: {
    maxFloors: 10,
    cars: 100
  },
  colors: ['red', 'green', 'blue', 'white', 'black'],
  schedule: {
    cron: '* * * * * *' // every second
  }
});