const Factory = require('./factory');
const Car = require('./car');

class CarFactory extends Factory {
  constructor() {
    super();

    this.supportedTypes = {
      random: true
    };
  }

  generate(type) {
    super.generate(type);

    if (type === 'random') {
      return this.createRandomCar();
    }
  }

  createRandomCar() {
    return new Car();
  }
}

module.exports = CarFactory;