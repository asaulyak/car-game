const schedule = require('node-schedule');

class Scheduler {
  constructor(cron) {
    this.cron = cron;
  }

  start(worker = () => {
  }) {
    this.job = schedule.scheduleJob(this.cron, (worker));
  }

  stop() {
    this.job && this.job.cancel();
  }
}

module.exports = Scheduler;
