const Scheduler = require('./scheduler');
const config = require('./config');

class Tracker {
  constructor() {
    this.scheduler = new Scheduler(config.schedule.cron);
  }

  track(garages = []) {
    this.scheduler.stop();

    this.garages = garages;

    this.initGarages(garages);

    this.scheduler.start(() => this.tick());
  }

  initGarages(garages) {
    garages.forEach(garage => {
      garage.on('move', event => this.onGarageMove(event, garage));
    });
  }

  tick() {
    if (!this.garages.length) {
      this.scheduler.stop();
    }
    else {
      this.garages.forEach(garage => garage.moveCars());
    }
  }

  onGarageMove(event, garage) {
    if (!event.length) {
      // Remove a garage from the tracking list
      this.garages.splice(this.garages.indexOf(garage), 1);
      console.log(`Garage ${garage.id} has finished moving`);
    }
    else {
      console.log(`Garage: ${garage.id} performed a move`);
      console.table(event);
    }
  }
}

module.exports = Tracker;