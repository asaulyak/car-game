const CarFactory = require('./car-factory');
const GarageFactory = require('./garage-factory');
const Tracker = require('./tracker');

const factory = new CarFactory();
const garageFactory = new GarageFactory();
const tracker = new Tracker();

const cars1 = [factory.generate('random'), factory.generate('random'), factory.generate('random'), factory.generate('random')];
const cars2 = [factory.generate('random'), factory.generate('random'), factory.generate('random'), factory.generate('random')];

const garage1 = garageFactory.generate('random');
const garage2 = garageFactory.generate('random');

garage1.receiveCars(cars1);
garage2.receiveCars(cars2);

tracker.track([garage1, garage2]);