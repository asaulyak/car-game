const uuid = require('uuid');
const config = require('./config');
const EventEmitter = require('events');

class Garage extends EventEmitter {
  constructor() {
    super();
    this.id = uuid.v4();
    this.floors = config.garage.maxFloors;
    this.carsAmount = config.garage.cars;
    this.store = [];
  }

  receiveCars(cars = []) {
    if (cars.length > this.carsAmount) {
      throw new Error(`Garage capacity allows to store not more than ${this.carsAmount} cars`);
    }

    this.fillSlots(cars);
  }

  moveCars() {
    const diff = this.toMove.map(slot => {
      const previousFloor = slot.floor;
      slot.floor = Math.min(this.floors, slot.car.target, slot.floor + slot.car.speed);

      return {
        carId: slot.car.id,
        color: slot.car.color,
        speed: slot.car.speed,
        target: slot.car.target,
        before: previousFloor,
        after: slot.floor
      };
    });

    this.notifyCarChange(diff);
  }

  fillSlots(cars = []) {
    // Store all the cars on the first floor initially
    this.store = cars
      .map(car => ({
        floor: 1,
        car
      }));
  }

  get toMove() {
    return this.store.filter(slot => slot.floor !== slot.car.target);
  }

  notifyCarChange(diff = []) {
    this.emit('move', diff);
  }

  print() {
    console.table(this.store.map(slot => ({
      floor: slot.floor,
      car: slot.car.id,
      target: slot.car.target,
      speed: slot.car.speed
    })));
  }
}

module.exports = Garage;