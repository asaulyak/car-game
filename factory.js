class Factory {
  constructor() {
    // Plain object to make type traverse complexity O(1)
    this.supportedTypes = {};
  }
  generate(type) {
    if(!this.supportedTypes[type]) {
      throw new Error('Unsupported entity type');
    }
  }
}

module.exports = Factory;