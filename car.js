const uuid = require('uuid');
const random = require('random');
const config = require('./config');

class Car {
  constructor() {
    this.id = uuid.v4();
    this.color = config.colors[random.int(0, config.colors.length - 1)];
    this.speed = random.int(1, config.garage.maxFloors); // Floors per tick
    this.target = random.int(1, config.garage.maxFloors);
  }
}

module.exports = Car;